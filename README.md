# Leigh - Yaël

Hi there ! 👋 \
My name is Yaël. I am junior developer who would love to explore the world of coding and expand my skills 🚀

I'm currently in training to become an application developer and conceptor at Toulouse! I'm a fast learner on many subjects, I enjoy coding and solving problems. I am also very creative and versatile. 💡

I may be junior in coding but I have many soft skills that will be very useful during my learning journey. I previously worked as a digital advisor and trainer for adults who had never used a computer. This work experience has teach me a lot. I learned to be more patient and a good instructor. I discovered that I had good social skills and can talk in front of an audience! 👩‍🎓  

Furthermore, I also enjoy digital painting and illustration on my free time. This hobby taught me how to use so many softwares and to search for documentation online. I am able to look online to find the solution to my problems. It's a good skill for learning art AND coding ! 🎨

I'm looking for an opportunity to improve my skills and to grow professionnaly so don't hesitate to reach to me ! I am available as an apprentice for Septembre 2024. 

Contact me 📧 yael.clavequin@gmail.com




